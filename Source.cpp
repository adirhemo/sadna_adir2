#include <windows.h>
#include <iostream>
#include "Helper.h"
#include <string>
#include <vector>
#include <tchar.h>

typedef int(__stdcall *f_funci)();

std::string CurrPath() {
	char buffer[MAX_PATH];
	if (!GetModuleFileName(NULL, buffer, MAX_PATH))
	{
		printf("Cannot (%d)\n", GetLastError());
	}
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	return std::string(buffer).substr(0, pos);
}

bool dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}

void secret()
{
	HINSTANCE hGetProcIDDLL = LoadLibrary("C:\\Users\\magshimim\\Documents\\Second Year\\cpp 2nd\\Lesson5\\Project1\\Debug\\SECRET.dll");

	if (!hGetProcIDDLL) {
		std::cout << "could not load the dynamic library" << std::endl;
	}

	// resolve function address here
	f_funci funci = (f_funci)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");
	if (!funci) {
		std::cout << "could not locate the function" << std::endl;
	}

	std::cout << "funci() returned " << funci() << std::endl;
}

void ls(std::string _dir)
{
	WIN32_FIND_DATA data;
	HANDLE hFind = FindFirstFile((_dir + "\\*").c_str(), &data);      // DIRECTORY

	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			std::cout << data.cFileName << std::endl;
		} while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}
}

void runExe(std::string cmd)
{
	//take care of exe
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	if (!CreateProcess(NULL,   // No module name (use command line)
		(LPSTR)cmd.c_str(),        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		//return;
	}

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);
	DWORD exitCode;
	GetExitCodeProcess(pi.hProcess, &exitCode);
	std::cout << "Exited with: " << exitCode << std::endl;
	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

}

void cd(std::vector<std::string> _words, std::string & _dir)
{
	std::string _tempDir(_words[1]);
	for (int i = 2; i < _words.size(); i++)
	{
		_tempDir = _tempDir + " " + _words[i];
	}
	if (dirExists(_tempDir))
	{
		_dir = _tempDir;
	}
	else
	{
		std::cout << "error:not a valid dir" << std::endl;
	}
}

void create(std::vector<std::string> _words, std::string _dir)
{
	std::string _temp(_dir + '\\' + _words[1]);
	for (int i = 2; i < _words.size(); i++)
	{
		_temp = _temp + " " + _words[i];
	}
	HANDLE hFile = CreateFile(
		_temp.c_str(),
		GENERIC_READ,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		std::cout << "error: unable to open file" << std::endl;
	}
	CloseHandle(hFile);
}

int main()
{
	std::string _cmd("");
	std::string _dir(CurrPath());
	std::vector<std::string> _words;
	while (true)
	{
		std::cout << ">>";
		std::getline(std::cin, _cmd);
		_words = Helper::get_words(_cmd);
		if (_words.size() == 1)
		{
			if (_words[0] == "pwd")
			{
				std::cout << _dir << std::endl;
			}
			if (_words[0] == "ls")
			{
				ls(_dir);
			}
			if (_words[0] == "secret")
			{
				secret();
			}
			if (_cmd.find(".exe") != std::string::npos)
			{
				runExe(_words[0]);
			}
			if (_words[0] == "cls")
			{
				system("cls");
			}
		}
		if (_words.size() > 1)
		{
			if (_words[0] == "cd")
			{
				cd(_words, _dir);
			}

			if (_words[0] == "create")
			{
				create(_words, _dir);
			}
		}
	}
	system("pause");
	return 0;
}